import numpy as np
from matplotlib import pyplot as plt

import tensorflow as tf
# from tensorflow.keras.layers import Input,Dense
# from tensorflow.keras.models import Model
# from tensorflow.keras.optimizers import Adam

def LSPlot(ls_vals):
    ls_vals = np.array(ls_vals)
    ndim = ls_vals.shape[1]
    fig,axes = plt.subplots(nrows=1,ncols=ndim)
    fig.set_size_inches(5*ndim,4)
    for dim in range(ndim):
        axes[dim].hist(ls_vals[:,dim],density=True,histtype='step')
        axes[dim].set_xlabel(f"LS dim. {dim}")

def showerPlot(showers, cell_eta, cell_phi):
    showers = np.array(showers)
    showers = showers.reshape(-1,7,7)
    fig,axes = plt.subplots(nrows=1,ncols=3)
    fig.set_size_inches(18,6)
    im = axes[0].imshow(showers[83])
    axes[0].set_title("Shower #83")

    axes[1].imshow(showers[900])
    axes[1].set_title("Shower #900")
    axes[2].imshow(tf.reduce_mean(showers,axis=0))
    axes[2].set_title("Avg. Shower")

    etalabels = ["{:.3f}".format(f) for f in np.unique(np.round(cell_eta,3))]
    philabels = ["{:.2f}".format(f) for f in np.unique(cell_phi)]

    for ax in axes:
        ax.set_xlabel("$\Delta\eta$ [mm]")
        ax.set_ylabel("$\Delta\phi$ [mm]")
        ax.set_xticks(range(len(etalabels)))
        ax.set_xticklabels(etalabels)
        ax.set_yticklabels([philabels[0]] + philabels)

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.83, 0.2, 0.03, 0.6])
    _=fig.colorbar(im, cax=cbar_ax)
